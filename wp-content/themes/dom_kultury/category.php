<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<section id="new">

    <div class="container">
        <h2>Aktualności</h2>
        <div class="row">
        <?php while (have_posts()) : the_post(); ?>

          <?php get_template_part('templates/front', 'content'); ?>

        <?php endwhile; ?>
        <?php the_posts_navigation(); ?>
        </div>
    </div>

</section>
