<section id="slider" class="rsDefault">
    <?php $slider = get_field('slider', 'option');
    if($slider) :

    foreach ($slider as $slide) { ?>
        <div class="slide" style="background-image: url(<?php echo esc_url($slide['image']['url']); ?>);">
            <div class="container">
                <div class="innerSlide">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo_slider.svg">
                </div>
            </div>
        </div>
    <?php } ?>

<?php endif; ?>
</section>
