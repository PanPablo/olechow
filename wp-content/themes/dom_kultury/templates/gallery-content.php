<?php
$galeria = get_field('galeria');
if ($galeria)
{
    $out = '';
    $out .= '<div class="gallery">';
    foreach ($galeria as $img)
    {
        # code...
        $out .= '<div class="gridItem">';


        $out .= '<a data-lightbox="galeria" href="' . esc_url($img['url']). '">';

            $out .= '<img class="img-fluid" src="' . esc_url($img['url']). '" alt="' . $img['alt']. '" title="' . $img['title']. '"/>';

        $out .= '</a>';

        $out .= '</div>';
    }

    $out .= '</div>';


    echo $out;
}
