<footer class="content-info">

  <div class="footerInfo">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-sm-6 col-xs-12 widgetFoot">
          <a href="#">
            <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/logo_footer.svg'; ?>" alt="OKG"/>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 col-xs-12 widgetFoot">
	        <?php the_field( 'footer_one', 'option' ); ?>
        </div>
        <div class="col-lg-6 col-sm-12 col-xs-12 widgetFoot">
          <div class="widget widget--padtop">
            <h3><span>Tel./Fax:</span> 42 684 66 47</h3>
            <h3><span>e-mail:</span> sekretariat@gorna.pl</h3>
	          <?php get_template_part( 'templates/social' ); ?>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-xs-12 widgetFoot">
          <div class="widget">
			  <?php $partners = get_field( 'partners', 'option' );

			  if ( $partners ) {
				  $out = '';
				  $out .= '<ul class="partners">';
				  foreach ( $partners as $s ) {
					  # code...
					  $out .= '<li>';

					  $out .= '<a target="_blank" href="' . $s['link'] . '">';
					  $out .= '<img class="img-fluid logos" src="' . esc_url( $s['ikona']['url'] ) . '" alt="' . $s['ikona']['alt'] . '" />';

					  $out .= '</a>';

					  $out .= '</li>';
				  }

				  $out .= '</ul>';
				  echo $out;
			  }

			  ?>
          </div>

        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="bottom-strap">
        <div class="container">

          <?php dynamic_sidebar( 'sidebar-footer' ); ?>

          <p>Ośrodek Kultury Górna 2017</p>

        </div>
      </div>
    </div>
  </div>

</footer>
