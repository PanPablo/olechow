<?php while (have_posts()) : the_post(); ?>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
  <article <?php post_class(); ?>>
    <header>
        <?php if ( function_exists('yoast_breadcrumb') ) {
        	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
      <?php get_template_part('templates/gallery', 'content'); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
</div>
<div class="col-lg-4 col-sm-4 col-sm-12 col-xs-12">
    <aside <?php post_class('postAside'); ?> data-postid="<?php the_ID(); ?>">
        <?php echo do_shortcode('[ssba]'); ?>
        <hr />
        <div class="info">
            <h5>Informacje: </h5>
            <dl>
                <?php $branch = wp_get_post_terms( $post->ID, 'branch');
                if($branch) {
                    echo '<dt>Dla kogo?</dt>';
                    echo '<dd>';
                    foreach ($branch as $term) {
                        # code...
                        //var_dump($term);
                         echo '<li class="' . $term->slug . '">'. $term->name . '</li>';
                    }
                    echo '</dd>';
                }
                ?>
                <?php $terms = wp_get_post_terms( $post->ID, 'localization');
                if($terms) {
                    echo '<dt>Gdzie?</dt>';
                    echo '<dd>';
                    foreach ($terms as $term) {
                        # code...
                         echo '<li>'. $term->description . '</li>';
                    }
                    echo '</dd>';
                }
                ?>
                <?php
                $data = get_field('data');
                $cyclical_event = get_field('cyclical_event');
                $bilety = get_field('bilety');
                $informacje = get_field('informacje');

                if($data) {
                    echo '<dt>Kiedy?</dt>';
                    echo '<dd>' . $data . '</dd>';
                }

                if($cyclical_event) {
                    echo '<dt >Nadchodzące wydarzenia: </dt>';
                    echo '<dd id="when"></dd>';

                }

                if($bilety) {
                    echo '<dt >Bilety: </dt>';
                    echo '<dd>' . $bilety . '</dd>';

                }

                if($informacje) {
                    echo '<dt >Info: </dt>';
                    echo '<dd>' . $informacje . '</dd>';

                }

                ?>
            </dl>
            <hr>
            <?php $animator = get_field('animator');

            if($animator) { ?>
            <h5>Twórcy/Autorzy</h5>
            <dl class="authors">
                <dt>Prowadzący:</dt>
                <?php
                echo '<dd>';
                foreach ($animator as $a) {
                    # code...
                    //var_dump($a);
                     echo '<li>' . $a['autor']->post_title . '</li>';
                }
                echo '</dd>';

                ?>

            </dl>
            <?php echo '<hr />'; } ?>

            <?php $custom_link = get_field('custom_link');
            if($custom_link) { ?>
                <h5>Zobacz</h5>

                <a href="<?php echo esc_url($custom_link); ?>">
                    <?php the_field('link_anchor_text'); ?>
                </a>

            <?php echo '<hr />'; } ?>

            <?php $partners = get_field('partners');
            if($partners) {

                $out = '';
                $out .= '<h5>Partnerzy</h5>';
                $out .= '<div class="partnersWrap">';
                foreach ($partners as $p) {
                    $out .= '<a href="' . esc_url($p['partner_link']). '">';
                        $out .= '<img class="img-fluid" src="' . esc_url($p['partner_logo']['url']). '" alt="' . $p['partner']. '" />';
                    $out .= '</a>';
                    # code...
                }
                $out .= '</div>';
                echo $out;
                echo '<hr />';
            }



            ?>


        </div>

    </aside>
</div>
<?php endwhile; ?>
</div>
