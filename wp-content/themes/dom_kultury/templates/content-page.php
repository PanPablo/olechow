<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
  <article <?php post_class(); ?>>
    <header>
        <?php if ( function_exists('yoast_breadcrumb') ) {
        	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
      <?php get_template_part('templates/gallery', 'content'); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
  </article>
</div>
<div class="col-lg-4 col-sm-4 col-sm-12 col-xs-12">
    <aside <?php post_class('postAside'); ?> data-postid="<?php the_ID(); ?>">
        <?php echo do_shortcode('[ssba]'); ?>
        <hr />
    </aside>
</div>
</div>

<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
