<?php $social = get_field('social', 'option');

if($social) {
    $out = '';
    $out .= '<ul>';
    foreach ($social as $s) {
        # code...
        $out .= '<li>';

          $out .= '<a target="_blank" href="' . $s['link'] . '">';
              $out .= '<img src="' . esc_url($s['ikona']['url']) . '" alt="' . $s['ikona']['alt']. '" />';

          $out .= '</a>';

        $out .= '</li>';
    }

    $out .= '</ul>';
    echo $out;
}

?>
