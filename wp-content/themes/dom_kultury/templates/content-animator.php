<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <header>
        <?php if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
        <h1>Zespoł</h1>

    </header>
</div>

<?php while (have_posts()) : the_post(); ?>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <article <?php post_class(); ?>>
      <h2 class="entry-title"><?php the_title(); ?></h2>

    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
  </article>
</div>
<div class="col-lg-8 col-sm-8 col-sm-12 col-xs-12">

<div class="map">

</div>

</div>
    <?php
    $departments = get_field('departments');

    if($departments) {
        $out = '';
        foreach ($departments as $dep) {
            # code...
            $out .= '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 deps">';
            $out .= '<h3>' . $dep['label']. '</h3>';
            $out .= $dep['responsible'] ? '<p>' . $dep['responsible']->post_title . '</p>' : '';
            $out .=  $dep['text'];

            $out .= '</div>';
        }

        echo $out;

    } ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <hr>
    </div>
<?php endwhile; ?>

</div>
