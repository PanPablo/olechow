<header class="banner">
    <footer id="footerInfo">
        <div class="container">
            <div class="headers">
                <hgroup>
                    <a class="brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?>
                    </a>
                    <span><?php the_field('top_contact_info', 'option'); ?></span>
                </hgroup>
                <?php get_template_part('templates/social'); ?>
            </div>
        </div>
    </footer>
  <div class="container">

    <nav class="mainNav">
      <?php
      if (has_nav_menu('branch_navigation')) :
        wp_nav_menu(['theme_location' => 'branch_navigation', 'container' => '','menu_class' => 'nav']);
        endif;
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'container' => '', 'menu_class' => 'nav']);
      endif;

      ?>
    </nav>
  </div>
</header>
<div class="clearfix" style="height: 117px;">

</div>
