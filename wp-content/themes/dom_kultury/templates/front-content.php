    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 frontWrap">
        <article class="frontArticle">
            <a class="mainLink" href="<?php the_permalink(); ?>">
            <div class="frontImage" style="background-image: url(<?php the_post_thumbnail_url('front'); ?>);">
            </div></a>
            <div class="frontText">
                <?php if ('event' == get_post_type()) {
                    $data_headline = get_field('data_headline');
                    $data_headline = $data_headline ? $data_headline : 'Wydarzenie';
                    echo '<address class="eventHead">' . $data_headline . '</address>';
                } ?>
                <div class="intro">

                    <?php
                    $locale = wp_get_post_terms( $post->ID, 'localization');
                    echo '<ul class="localization">';
                    if($locale)
                    {
                        foreach ($locale as $term) {
                            # code...
                            echo '<li class="' . $term->slug . '">' . $term->name . '</li>';
                        }
                    } else {
                        echo '<li class="">OKG</li>';
                    }
                    echo '</ul>';
                    ?>

                    <?php if (!'event' == get_post_type()) { ?>
                        <address class="postDate">
                        <?php the_date(); ?>
                        </address>
                    <?php } ?>
                    <a href="<?php the_permalink(); ?>">
                    <?php
                    the_title('<h3>', '</h3>');
                    the_excerpt();
                    ?>
                    </a>
                </div>
                <footer>
                    <div class="">
                    </div>
                <?php
                $terms = wp_get_post_terms( $post->ID, 'branch');
                if($terms)
                {
                    echo '<ul class="branches">';
                    foreach ($terms as $term) {
                        # code...
                        echo '<li class="' . $term->slug . '">' . $term->name . '</li>';
                    }
                    echo '</ul>';
                }
                ?>
                </footer>
            </div>
            </a>
        </article>
    </div>
