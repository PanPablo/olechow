
<section id="calendar">
    <header>
        <div class="container">
            <h2>Spotkajmy się w domu kultury</h2>
        </div>
    </header>
    <div id="calendarInner">
        <div class="hello">
        </div>



<!--<?php //get_template_part('templates/loader', 'img.svg'); ?>-->
    </div>
</div>
</section>

<?php

# Banner section

$banner_text = get_field('banner_text', 'option');
$banner_img = get_field('banner_img', 'option');

$out = '';

$out .= '<section class="banner2">';

  $out .= '<div class="container">';

    $out .= '<div class="row">';

      $out .= '<div class="col-sm-4 col-lg-2">';

        $out .= '<div class="banner2__text">';

          $out .= $banner_text;

        $out .= '</div>';

      $out .= '</div>';

      $out .= '<div class="col-sm-8 col-lg-10">';

        $out .= '<div class="banner2__img">';

          $out .= '<img class="img-fluid" src="' . $banner_img["url"] . '">';

        $out .= '</div>';

      $out .= '</div>';

    $out .= '</div>';

  $out .= '</div>';

$out .= '</section>';


if ( $banner_text && $banner_img )
  echo $out;


?>

<section id="news">
    <div class="container">
        <h2>Aktualności</h2>
        <div class="row">
        <?php query_posts(array( 'cat' => '2' )); ?>
        <?php while (have_posts()) : the_post(); ?>
          <?php get_template_part('templates/front', 'content'); ?>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>
        </div>
    </div>
</section>
