/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
(function($) {
  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
        $('#slider').slick({
          arrows: false,
        });
        $.get(path.calendar, {per_page: 100}, function(templates) {
          //console.log(path.events);
          //console.log(path.events);
          var dates = function() {
            $.get(path.events, {per_page: 100}, function(d) {
              ////console.log(d);
              return d;
            }).done(function(d) {
              //Create events array
              var allEvents = [];
              //Recreate from wp rest api
              d.forEach(function(element, index) {
                //console.log(element);
                if (element.acf.data) {
                  var done = {};
                  done.date = moment(element.acf.data, "YYYY-MM-DD HH:mm").unix();
                  done.title = element.title.rendered;
                  done.cycle = false;
                  done.post_id = element.id;
                  done.end_date = element.acf.end_date ? moment(element.acf.end_date, "YYYY-MM-DD HH:mm").unix() : moment(element.acf.data, "YYYY-MM-DD HH:mm").unix();
                  allEvents.push(done);
                }
                console.log(element.acf);
                if (element.acf.cyclical_event) {
                  var title = element.title.rendered;
                  var idy = element.id;
                  element.acf.cyclical_event.forEach(function(t) {
                    var excepts = [];

                    for (var i = 0; i < 10; i++) {

                      var done = {};
                      //var tempData = t.data;
                      done.valid = 'valid';


                      //console.log(t.data);
                      var toAddData = moment(t.data, "YYYY-MM-DD HH:mm").unix();
                      var toAddEnd = t.end_date ? moment(t.end_date, "YYYY-MM-DD HH:mm").unix() : moment(t.data, "YYYY-MM-DD HH:mm").unix();
                      toAddEnd = toAddData + i * 604800;
                      var times = toAddData + i * 604800;

                     // console.log(times);

                      //console.log(toAddEnd);
                      done.date = times;
                      done.end_date = toAddEnd;
                      done.title = title;
                      done.cycle = true;
                      done.post_id = idy;

                      var tempD = t.data;
                      for (var y = 0; y < t.exception.length; y++) {

                        if (moment(t.exception[y].excep, "YYYY-MM-DD HH:mm").unix() === times) {
                          done.valid = "notvalid";
                        }
                      }
                      allEvents.push(done);
                    }
                  });
                }
              });
              console.log(allEvents);
              //////console.log(allEvents);
              //Make comparison
              function compare(a, b) {
                if (a.date < b.date) {
                  return -1;
                }
                if (a.date > b.date) {
                  return 1;
                }
                return 0;
              }
              //Sort Events
              allEvents.sort(compare);
              ////console.log(allEvents);
              var final = [];
              //var multiDays = [];
              allEvents.forEach(function(el, index) {
                var ex = {};
                var dateString = moment.unix(el.date).format("YYYY-MM-DD HH:mm");
                var dateUnix = moment.unix(el.date);
                var dateStringEnd = moment.unix(el.end_date).format("YYYY-MM-DD HH:mm");
                var dateUnixEnd = moment.unix(el.end_date);
                ////console.log(dateUnixEnd);


                ex.date = dateString;
                ex.end_date = dateStringEnd;

                ex.title = el.title;
                ex.valid = el.valid;
                ex.cycle = el.cycle;
                ex.post_id = el.post_id;
                ex.id = 'id_' + index;
                ex.dateUnix = dateUnix;
                ex.dateUnixEnd = dateUnixEnd;
                final.push(ex);
              });
              //console.log(final);

              //console.log(final);
              ////console.log(multiDays);
              moment.locale('pl');
              $('#calendarInner').clndr({
                template: templates,
                daysOfTheWeek: ['Pon', 'Wt', 'Śr', 'Czw', 'Pt', 'So', 'Niedz'],
                events: final,
                trackSelectedDate: true,
                multiDayEvents: {
                    startDate: 'date',
                    endDate: 'end_date',
                },
                extras: {
                  today: moment().startOf('day').unix().valueOf(),
                },
                clickEvents: {
                  click: function(day) {
                    console.log(day);
                    var eve = day.events;
                    console.log(eve);
                    $('.event-item').each(function() {
                      $(this).removeClass('showEvents');
                    });
                    eve.forEach(function(el) {
                      $('.' + el.id).addClass('showEvents');
                    });
                  },

                  nextMonth: function(month) {
                    var currentMonth = moment().month();
                    var currentYear = moment().year();
                    var thisMonth = moment(month._d).month();
                    var thisYear = moment(month._d).year();
                    if (currentMonth === thisMonth && currentYear === thisYear) {} else {
                      ////console.log(month._isValid);
                      ////console.log(month);
                      var dataToCheck = moment(month._d).valueOf();
                      ////console.log(dataToCheck);
                      $('[data-date="' + dataToCheck + '"]').addClass('today');
                      final.forEach(function(el) {
                        var currentCheck = el.dateUnix.valueOf();
                        if (currentCheck >= dataToCheck && currentCheck < dataToCheck + 86400000) {
                          $('.' + el.id).addClass('showEvents');
                        }
                      });
                    }
                  },
                  previousMonth: function(month) {
                    var currentMonth = moment().month();
                    var currentYear = moment().year();
                    var thisMonth = moment(month._d).month();
                    var thisYear = moment(month._d).year();
                    if (currentMonth === thisMonth && currentYear === thisYear) {} else {
                      ////console.log(month._isValid);
                      ////console.log(month);
                      var dataToCheck = moment(month._d).endOf('month').startOf('day').valueOf();
                      ////console.log(dataToCheck);
                      $('[data-date="' + dataToCheck + '"]').addClass('today');
                      final.forEach(function(el) {
                        var currentCheck = el.dateUnix.valueOf();
                        if (currentCheck >= dataToCheck && currentCheck < dataToCheck + 86400000) {
                          $('.' + el.id).addClass('showEvents');
                        }
                      });
                    }
                  }
                }
              });
            });
          };
          dates();
        });
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS

      }
    },
    'single': {
      init: function() {

        var dates = function() {
          $.get(path.events, function(d) {
            ////console.log(d);
            return d;
          }).done(function(d) { //Create events array
            var allEvents = [];
            //Recreate from wp rest api
            d.forEach(function(element, index) {
              //////console.log(i);
              if (element.acf.data) {
                var done = {};
                done.date = moment(element.acf.data, "YYYY-MM-DD HH:mm").unix();
                done.title = element.title.rendered;
                done.cycle = false;
                done.post_id = element.id;
                allEvents.push(done);
              }
              if (element.acf.cyclical_event) {
                var title = element.title.rendered;
                var idy = element.id;
                element.acf.cyclical_event.forEach(function(t) {
                  var excepts = [];

                  // t.exception.forEach(function(exx) {
                  //
                  //     excepts.push(exx.except);
                  //
                  // });
                  ////console.log(excepts);


                  for (var i = 1; i < 10; i++) {

                    var done = {};
                    //var tempData = t.data;
                    done.valid = 'valid';


                    //////console.log(t.data);
                    toAddData = moment(t.data, "YYYY-MM-DD HH:mm").unix();
                    var times = toAddData + i * 604800;
                    //////console.log(times);
                    done.date = times;
                    done.title = title;
                    done.cycle = true;
                    done.post_id = idy;
                    ////console.log(done);
                    //Check exceptions
                    var tempD = t.data;
                    for (var y = 0; y < t.exception.length; y++) {
                      ////console.log(tempD);
                      ////console.log(t.exception[y].excep);
                      if (moment(t.exception[y].excep, "YYYY-MM-DD HH:mm").unix() === times) {
                        done.valid = "notvalid";
                      }
                    }
                    allEvents.push(done);
                  }
                });
              }
            });
            //////console.log(allEvents);
            //Make comparison
            function compare(a, b) {
              if (a.date < b.date) {
                return -1;
              }
              if (a.date > b.date) {
                return 1;
              }
              return 0;
            }
            //Sort Events
            allEvents.sort(compare);
            ////console.log(allEvents);
            var final = [];
            allEvents.forEach(function(el, index) {
              var ex = {};
              var dateString = moment.unix(el.date).format("YYYY-MM-DD HH:mm");
              var dateUnix = moment.unix(el.date);
              //////console.log(dateString);
              ex.date = dateString;
              ex.title = el.title;
              ex.valid = el.valid;
              ex.cycle = el.cycle;
              ex.post_id = el.post_id;
              ex.id = 'id_' + index;
              ex.dateUnix = dateUnix;
              final.push(ex);
            });
            ////console.log(final);
            moment.locale('pl');
            ////console.log(final);

            var cycleTemplate = '<% _.each(eventsThisInterval, function(event) { if(extras.thisID == event.post_id) { %><span class="<%= extras.thisID %> <%= event.valid %>"><%= moment(event.date, "YYYY-MM-DD HH:mm").format("ddd") %> <%= moment(event.date, "YYYY-MM-DD HH:mm").format("HH:mm DD.MM.YYYY") %></span><%  } }); %>';
            $('#when').clndr({
              template: cycleTemplate,
              trackSelectedDate: true,
              daysOfTheWeek: ['Pon', 'Wt', 'Śr', 'Czw', 'Pt', 'So', 'Niedz'],
              lengthOfTime: {
                // Set to an integer if you want to render one or more months, otherwise
                // leave this null
                months: null,

                // Set to an integer if you want to render one or more days, otherwise
                // leave this null. Setting this to 14 would render a 2-week calendar.
                days: 66,

                // This is the amount of months or days that will move forward/back when
                // paging the calendar. With days=14 and interval=7, you would have a
                // 2-week calendar that pages forward and backward 1 week at a time.
                interval: 7
              },
              events: final,
              intervalEnd: moment().add(3, 'months'),
              intervalStart: moment(),
              eventsThisInterval: final,

              extras: {
                today: moment().startOf('day').unix().valueOf(),
                thisID: $('aside').attr('data-postid')
              },
            });
          });
        };
        dates();
    },
    finalize: function() {
        $('.gallery').masonry({
          itemSelector: '.gridItem',
          columnWidth: 0
        });
    }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
  },
  'archive': {
      init: function() {
        // JavaScript to be fired on the about us page
        //console.log("działam");
        function initMap(ll, div, add) {
            var styledMapType = new google.maps.StyledMapType(
             [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":40}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-10},{"lightness":30}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":10}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":60}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]}], {name: 'Styled Map'});
           var uluru = ll;
           var map = new google.maps.Map(document.querySelectorAll('.map')[div], {
             zoom: 14,
             center: uluru,
             mapTypeControlOptions: {
                mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                        'styled_map']
              }

           });
           map.mapTypes.set('styled_map', styledMapType);
           map.setMapTypeId('styled_map');

           var marker = new google.maps.Marker({
             position: uluru,
             map: map
           });
           if(add) {
               var markery = new google.maps.Marker({
                 position: add,
                 map: map
               });
           }
         }

         var gorna = {lat: 51.740855, lng: 19.480122};
         var olechow = {lat: 51.741732, lng: 19.561378};
         var olechow2 = {lat: 51.743582, lng: 19.563002};
         initMap(gorna,0);
         initMap(olechow,1, olechow2);

      }
  }
  };
  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';
      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');
      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });
      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };
  // Load Events
  $(document).ready(UTIL.loadEvents);
})(jQuery); // Fully reference jQuery after this point.
