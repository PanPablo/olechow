<?php

function localization_init() {
	register_taxonomy( 'localization', array( 'post', 'event' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Localizations', 'okg' ),
			'singular_name'              => _x( 'Localization', 'taxonomy general name', 'okg' ),
			'search_items'               => __( 'Search localizations', 'okg' ),
			'popular_items'              => __( 'Popular localizations', 'okg' ),
			'all_items'                  => __( 'All localizations', 'okg' ),
			'parent_item'                => __( 'Parent localization', 'okg' ),
			'parent_item_colon'          => __( 'Parent localization:', 'okg' ),
			'edit_item'                  => __( 'Edit localization', 'okg' ),
			'update_item'                => __( 'Update localization', 'okg' ),
			'add_new_item'               => __( 'New localization', 'okg' ),
			'new_item_name'              => __( 'New localization', 'okg' ),
			'separate_items_with_commas' => __( 'Separate localizations with commas', 'okg' ),
			'add_or_remove_items'        => __( 'Add or remove localizations', 'okg' ),
			'choose_from_most_used'      => __( 'Choose from the most used localizations', 'okg' ),
			'not_found'                  => __( 'No localizations found.', 'okg' ),
			'menu_name'                  => __( 'Localizations', 'okg' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'localization',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'localization_init' );
