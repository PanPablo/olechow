<?php
function branch_init() {
	register_taxonomy( 'branch', array( 'post', 'event' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Branches', 'okg' ),
			'singular_name'              => _x( 'Branch', 'taxonomy general name', 'okg' ),
			'search_items'               => __( 'Search branches', 'okg' ),
			'popular_items'              => __( 'Popular branches', 'okg' ),
			'all_items'                  => __( 'All branches', 'okg' ),
			'parent_item'                => __( 'Parent branch', 'okg' ),
			'parent_item_colon'          => __( 'Parent branch:', 'okg' ),
			'edit_item'                  => __( 'Edit branch', 'okg' ),
			'update_item'                => __( 'Update branch', 'okg' ),
			'add_new_item'               => __( 'New branch', 'okg' ),
			'new_item_name'              => __( 'New branch', 'okg' ),
			'separate_items_with_commas' => __( 'Separate branches with commas', 'okg' ),
			'add_or_remove_items'        => __( 'Add or remove branches', 'okg' ),
			'choose_from_most_used'      => __( 'Choose from the most used branches', 'okg' ),
			'not_found'                  => __( 'No branches found.', 'okg' ),
			'menu_name'                  => __( 'Branches', 'okg' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'branch',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'branch_init' );
