<?php

function Cycle_init() {
	register_taxonomy( 'cycle', array( 'event' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Cycles', 'okg' ),
			'singular_name'              => _x( 'Cycle', 'taxonomy general name', 'okg' ),
			'search_items'               => __( 'Search cycles', 'okg' ),
			'popular_items'              => __( 'Popular cycles', 'okg' ),
			'all_items'                  => __( 'All cycles', 'okg' ),
			'parent_item'                => __( 'Parent cycle', 'okg' ),
			'parent_item_colon'          => __( 'Parent cycle:', 'okg' ),
			'edit_item'                  => __( 'Edit cycle', 'okg' ),
			'update_item'                => __( 'Update cycle', 'okg' ),
			'add_new_item'               => __( 'New cycle', 'okg' ),
			'new_item_name'              => __( 'New cycle', 'okg' ),
			'separate_items_with_commas' => __( 'Separate cycles with commas', 'okg' ),
			'add_or_remove_items'        => __( 'Add or remove cycles', 'okg' ),
			'choose_from_most_used'      => __( 'Choose from the most used cycles', 'okg' ),
			'not_found'                  => __( 'No cycles found.', 'okg' ),
			'menu_name'                  => __( 'Cycles', 'okg' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'Cycle',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'Cycle_init' );
