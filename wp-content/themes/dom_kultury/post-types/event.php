<?php

function event_init() {
	register_post_type( 'event', array(
		'labels'            => array(
			'name'                => __( 'Events', 'okg' ),
			'singular_name'       => __( 'Event', 'okg' ),
			'all_items'           => __( 'All Events', 'okg' ),
			'new_item'            => __( 'New event', 'okg' ),
			'add_new'             => __( 'Add New', 'okg' ),
			'add_new_item'        => __( 'Add New event', 'okg' ),
			'edit_item'           => __( 'Edit event', 'okg' ),
			'view_item'           => __( 'View event', 'okg' ),
			'search_items'        => __( 'Search events', 'okg' ),
			'not_found'           => __( 'No events found', 'okg' ),
			'not_found_in_trash'  => __( 'No events found in trash', 'okg' ),
			'parent_item_colon'   => __( 'Parent event', 'okg' ),
			'menu_name'           => __( 'Events', 'okg' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-calendar-alt',
		'show_in_rest'      => true,
		'rest_base'         => 'event',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'event_init' );

function event_updated_messages( $messages )
{
	global $post;

	$permalink = get_permalink( $post );

	$messages['event'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Event updated. <a target="_blank" href="%s">View event</a>', 'okg'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'okg'),
		3 => __('Custom field deleted.', 'okg'),
		4 => __('Event updated.', 'okg'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Event restored to revision from %s', 'okg'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Event published. <a href="%s">View event</a>', 'okg'), esc_url( $permalink ) ),
		7 => __('Event saved.', 'okg'),
		8 => sprintf( __('Event submitted. <a target="_blank" href="%s">Preview event</a>', 'okg'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Event scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview event</a>', 'okg'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Event draft updated. <a target="_blank" href="%s">Preview event</a>', 'okg'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'event_updated_messages' );
