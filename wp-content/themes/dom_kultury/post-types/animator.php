<?php

function animator_init() {
	register_post_type( 'animator', array(
		'labels'            => array(
			'name'                => __( 'Animators', 'okg' ),
			'singular_name'       => __( 'Animator', 'okg' ),
			'all_items'           => __( 'All Animators', 'okg' ),
			'new_item'            => __( 'New animator', 'okg' ),
			'add_new'             => __( 'Add New', 'okg' ),
			'add_new_item'        => __( 'Add New animator', 'okg' ),
			'edit_item'           => __( 'Edit animator', 'okg' ),
			'view_item'           => __( 'View animator', 'okg' ),
			'search_items'        => __( 'Search animators', 'okg' ),
			'not_found'           => __( 'No animators found', 'okg' ),
			'not_found_in_trash'  => __( 'No animators found in trash', 'okg' ),
			'parent_item_colon'   => __( 'Parent animator', 'okg' ),
			'menu_name'           => __( 'Animators', 'okg' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-businessman',
		'show_in_rest'      => true,
		'rest_base'         => 'animator',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'animator_init' );

function animator_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['animator'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Animator updated. <a target="_blank" href="%s">View animator</a>', 'okg'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'okg'),
		3 => __('Custom field deleted.', 'okg'),
		4 => __('Animator updated.', 'okg'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Animator restored to revision from %s', 'okg'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Animator published. <a href="%s">View animator</a>', 'okg'), esc_url( $permalink ) ),
		7 => __('Animator saved.', 'okg'),
		8 => sprintf( __('Animator submitted. <a target="_blank" href="%s">Preview animator</a>', 'okg'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Animator scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview animator</a>', 'okg'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Animator draft updated. <a target="_blank" href="%s">Preview animator</a>', 'okg'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'animator_updated_messages' );
