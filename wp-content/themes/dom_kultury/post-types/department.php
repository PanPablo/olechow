<?php

function department_init() {
	register_post_type( 'department', array(
		'labels'            => array(
			'name'                => __( 'Departments', 'okg' ),
			'singular_name'       => __( 'Department', 'okg' ),
			'all_items'           => __( 'All Departments', 'okg' ),
			'new_item'            => __( 'New department', 'okg' ),
			'add_new'             => __( 'Add New', 'okg' ),
			'add_new_item'        => __( 'Add New department', 'okg' ),
			'edit_item'           => __( 'Edit department', 'okg' ),
			'view_item'           => __( 'View department', 'okg' ),
			'search_items'        => __( 'Search departments', 'okg' ),
			'not_found'           => __( 'No departments found', 'okg' ),
			'not_found_in_trash'  => __( 'No departments found in trash', 'okg' ),
			'parent_item_colon'   => __( 'Parent department', 'okg' ),
			'menu_name'           => __( 'Departments', 'okg' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-location-alt',
		'show_in_rest'      => true,
		'rest_base'         => 'department',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'department_init' );

function department_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['department'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Department updated. <a target="_blank" href="%s">View department</a>', 'okg'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'okg'),
		3 => __('Custom field deleted.', 'okg'),
		4 => __('Department updated.', 'okg'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Department restored to revision from %s', 'okg'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Department published. <a href="%s">View department</a>', 'okg'), esc_url( $permalink ) ),
		7 => __('Department saved.', 'okg'),
		8 => sprintf( __('Department submitted. <a target="_blank" href="%s">Preview department</a>', 'okg'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Department scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview department</a>', 'okg'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Department draft updated. <a target="_blank" href="%s">Preview department</a>', 'okg'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'department_updated_messages' );
