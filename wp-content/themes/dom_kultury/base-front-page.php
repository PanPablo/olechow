<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
      if(is_front_page()) {
          get_template_part('templates/slider');
      }
    ?>
    <div class="wrap" role="document">
      <div class="content">
        <main>
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <nav id="cycleEvents">
        <div class="container">
            <div class="row">
            <?php query_posts(array(
                'post_type' => 'event',
                'posts_per_page' => -1,
                'tax_query' => array(
        	          array(
                          'taxonomy' => 'cycle',
                          'field'    => 'slug',
                          'terms'    => 'wydarzenie-cykliczne',
                            ),
                        ),
                    )
                ); ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <!-- post -->

                <?php

                $cycle_thumb = get_field('cycle_thumb');
                if ($cycle_thumb) { ?>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <a href="<?php esc_url(the_permalink()); ?>">
                        <img class="img-fluid" src="<?php echo esc_url($cycle_thumb['url']); ?>" alt="<?php echo $cycle_thumb['alt']; ?>" />
                        </a>
                    </div>
                <?php } ?>

            <?php endwhile; ?>
                <!-- post navigation -->
            <?php else: ?>
                <!-- no posts found -->
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>

    </nav>
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
